//
//  RSBTestRequestHeaders.m
//  KosHero
//
//  Created by Anton Kovalev on 04.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBTestRequestHeaders.h"

@implementation RSBTestRequestHeaders

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];
    [mapping mapKeyPath:@"someValue" toProperty:@"someValue"];
    [mapping mapKeyPath:@"otherValue" toProperty:@"otherValue"];
    return mapping;
}

@end
