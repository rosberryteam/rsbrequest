//
//  RSBTestRequestHeaders.h
//  KosHero
//
//  Created by Anton Kovalev on 04.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBBaseRequestHeaders.h"

@interface RSBTestRequestHeaders : RSBBaseRequestHeaders

@property (nonatomic) NSString *someValue;
@property (nonatomic) NSString *otherValue;

@end
