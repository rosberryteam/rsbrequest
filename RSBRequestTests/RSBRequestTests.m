//
//  RSBRequestTests.m
//  KosHero
//
//  Created by Anton Kovalev on 03.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBBaseTests.h"
#import "RSBRequest.h"
#import <AFNetworking/AFNetworking.h>
#import <OHHTTPStubs.h>
#import <OHHTTPStubsResponse+JSON.h>
#import <OCMock.h>

#import "RSBResponse.h"
#import "RSBTestResponseBuilder.h"
#import "RSBWrongResponseBuilder.h"

#import "RSBTestRequestHeaders.h"
#import "RSBResponseErrorsHandler.h"
#import "RSBResponseValidator.h"
#import "RSBBaseRequestParameters.h"

@interface RSBRequestTests : RSBBaseTests

@property (nonatomic) RSBRequest<RSBResponse *> *request;
@property (nonatomic) id<RSBResponseBuilder> responseBuilder;
@property (nonatomic) id<RSBResponseBuilder> wrongResponseBuilder;
@property (nonatomic) RSBTestRequestHeaders *requestHeaders;
@property (nonatomic) AFHTTPSessionManager *sessionManager;
@property (nonatomic) id<RSBResponseErrorsHandler> errorsHandler;
@property (nonatomic) id<RSBResponseValidator> responseValidator;

@end

@implementation RSBRequestTests

- (void)setUp {
    [super setUp];
    
    self.request = [RSBRequest request];
    self.sessionManager = [AFHTTPSessionManager manager];
    self.request.sessionManager = self.sessionManager;
    self.responseBuilder = [[RSBTestResponseBuilder alloc] init];
    self.request.responseBuilder = self.responseBuilder;
    self.wrongResponseBuilder = [[RSBWrongResponseBuilder alloc] init];
    
    self.requestHeaders = [[RSBTestRequestHeaders alloc] init];
    self.requestHeaders.someValue = @"some_value";
    self.requestHeaders.otherValue = @"other_value";
    self.request.headers = self.requestHeaders;
    
    self.errorsHandler = OCMProtocolMock(@protocol(RSBResponseErrorsHandler));
    self.request.responseErrorsHandlers = @[self.errorsHandler];
    
    self.responseValidator = OCMProtocolMock(@protocol(RSBResponseValidator));
    self.request.responseValidators = @[self.responseValidator];
}

- (void)tearDown {
    self.errorsHandler = nil;
    self.requestHeaders = nil;
    self.sessionManager = nil;
    self.responseBuilder = nil;
    self.wrongResponseBuilder = nil;
    self.responseValidator = nil;
    self.request = nil;
    
    [super tearDown];
}

#pragma mark - Test different request methods

- (void)testThatRequestWorksWithGETMethod {
    NSString *method = @"GET";
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        [self.request performWithMethod:method path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatRequestWorksWithPOSTMethod {
    NSString *method = @"POST";
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        [self.request performWithMethod:method path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatRequestWorksWithPOSTMethodAndUseBodyConstructionBlockFromParameters {
    NSString *method = @"POST";
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        id mockParameters = OCMPartialMock([RSBBaseRequestParameters parameters]);
        [[[mockParameters stub] andReturn:^(id<AFMultipartFormData> data) {}] bodyConstructionBlock];
        self.request.parameters = mockParameters;
        
        [self.request performWithMethod:method path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatRequestWorksWithPUTMethod {
    NSString *method = @"PUT";
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        [self.request performWithMethod:method path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatRequestWorksWithPATCHMethod {
    NSString *method = @"PATCH";
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        [self.request performWithMethod:method path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatRequestWorksWithDELETEMethod {
    NSString *method = @"DELETE";
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        [self.request performWithMethod:method path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatRequestNotWorksWithUnknownMethod {
    NSString *method = @"SOME_UNKNOWN";
    XCTAssertThrows([self.request performWithMethod:method path:@"http://google.com" completionBlock:^(id  _Nullable response, NSError * _Nullable error) {}]);
}

#pragma mark - Test Response building

- (void)testThatRequestWithResponseBuilderCreateResponse {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            XCTAssertNil(error);
            XCTAssertEqual(response.class, [RSBResponse class]);
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatRequestWithWrongResponseBuilderCreateWrongResponse {
    self.request.responseBuilder = self.wrongResponseBuilder;
    
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        __weak typeof(self) weakSelf = self;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            XCTAssertNil(error);
            XCTAssertNotEqual(response.class, [RSBResponse class]);
            weakSelf.request.responseBuilder = weakSelf.responseBuilder;
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

#pragma mark - Request headers

- (void)testThatRequestHeadersCorrectlyWorks {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        __weak typeof(self) weakSelf = self;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            NSDictionary *actualHeaders = weakSelf.sessionManager.requestSerializer.HTTPRequestHeaders;
            NSDictionary *responseHeaders = [weakSelf.requestHeaders dictionaryRepresentation];
            for (NSString *key in responseHeaders.allKeys) {
                XCTAssertNotNil(actualHeaders[key]);
                XCTAssertEqual(actualHeaders[key], responseHeaders[key]);
            }
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

#pragma mark - Error handlers

- (void)testThatResponseErrorHandlersCalled {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        __weak typeof(self) weakSelf = self;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            OCMVerify([weakSelf.errorsHandler handleError:[OCMArg anyObjectRef] forRequest:[OCMArg isNotNil] fromTask:[OCMArg isNotNil]]);
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatIfResponseErrorsHandlerHandleErrorThenRequestCompletionBlockWillNotCalled {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        OCMStub([self.errorsHandler handleError:[OCMArg anyObjectRef] forRequest:[OCMArg isNotNil] fromTask:[OCMArg isNotNil]]).andReturn(YES);
        OCMExpect([self.errorsHandler handleError:[OCMArg anyObjectRef] forRequest:[OCMArg isNotNil] fromTask:[OCMArg isNotNil]]);

        __block BOOL requestCompletionNotCalled = YES;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            requestCompletionNotCalled = NO;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            OCMVerify(self.errorsHandler);
            XCTAssertEqual(requestCompletionNotCalled, YES);
            
            [(id)self.errorsHandler stopMocking];
            self.errorsHandler = OCMProtocolMock(@protocol(RSBResponseErrorsHandler));
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        });
    }];
}

#pragma mark - Cancel

- (void)testThatCancelWorks {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        
        __block BOOL requestCompletionNotCalled = YES;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            requestCompletionNotCalled = NO;
        }];
        [self.request cancel];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        });
    }];
}

#pragma mark - Response validators

- (void)testThatResponseValidatorsCalled {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        __weak typeof(self) weakSelf = self;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            OCMVerify([weakSelf.responseValidator validateResponse:response]);
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatIfResponseValidatorOccurErrorThenErrorHandlerWillBeCalled {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        NSError *error = [NSError errorWithDomain:@"" code:-1 userInfo:nil];
        OCMStub([self.responseValidator validateResponse:[OCMArg isNotNil]]).andReturn(error);
        
        __weak typeof(self) weakSelf = self;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            OCMVerify([weakSelf.errorsHandler handleError:[OCMArg anyObjectRef] forRequest:[OCMArg isNotNil] fromTask:[OCMArg isNotNil]]);
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatIfResponseValidatorOccurErrorAndResponseErrorsHandlerTooThenRequestCompletionBlockWillNotCalled {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        NSError *error = [NSError errorWithDomain:@"" code:-1 userInfo:nil];
        OCMStub([self.responseValidator validateResponse:[OCMArg isNotNil]]).andReturn(error);
        OCMExpect([self.responseValidator validateResponse:[OCMArg isNotNil]]);
        OCMStub([self.errorsHandler handleError:[OCMArg anyObjectRef] forRequest:[OCMArg isNotNil] fromTask:[OCMArg isNotNil]]).andReturn(YES);
        OCMExpect([self.errorsHandler handleError:[OCMArg anyObjectRef] forRequest:[OCMArg isNotNil] fromTask:[OCMArg isNotNil]]);
        
        __block BOOL requestCompletionNotCalled = YES;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            requestCompletionNotCalled = NO;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            OCMVerify(self.responseValidator);
            OCMVerify(self.errorsHandler);
            XCTAssertEqual(requestCompletionNotCalled, YES);
            
            [(id)self.errorsHandler stopMocking];
            self.errorsHandler = OCMProtocolMock(@protocol(RSBResponseErrorsHandler));
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        });
    }];
}

#pragma mark - Skip validation

- (void)testSkipValidationForValidResponse {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:200 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        self.request.shouldSkipValidation = YES;
        __weak typeof(self) weakSelf = self;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            XCTAssertNil(error);
            weakSelf.request.shouldSkipValidation = NO;
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testSkipValidationForInvalidResponse {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:500 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        self.request.shouldSkipValidation = YES;
        __weak typeof(self) weakSelf = self;
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            XCTAssertNil(response);
            weakSelf.request.shouldSkipValidation = NO;
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatCompletionBlockWillBeCalledWithErrorIfRequestFails {
    id<OHHTTPStubsDescriptor> descriptor = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:@{} statusCode:500 headers:nil]];
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(RSBResponse * _Nullable response, NSError * _Nullable error) {
            XCTAssertNotNil(error);
            [OHHTTPStubs removeStub:descriptor];
            [expectation fulfill];
        }];
    }];
}

@end
