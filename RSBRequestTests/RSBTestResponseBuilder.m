//
//  RSBResponseBuilder.m
//  KosHero
//
//  Created by Anton Kovalev on 04.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBTestResponseBuilder.h"
#import "RSBResponse.h"

@implementation RSBTestResponseBuilder

- (id)buildResponseWithResponseObject:(id)responseObject {
    RSBResponse *response = [[RSBResponse alloc] init];
    response.object = responseObject;
    return response;
}

@end
