//
//  RSBBaseTests.m
//  KosHero
//
//  Created by Anton Kovalev on 03.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBBaseTests.h"
#import <OHHTTPStubs.h>

@implementation RSBBaseTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (id<OHHTTPStubsDescriptor>)stubHTTPRequestsWithResponse:(OHHTTPStubsResponse *)response {
    return [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return YES;
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        return response;
    }];
}

- (void)performAsyncTestWithDescription:(NSString *)description waitTime:(NSTimeInterval)wait testBlock:(void(^)(XCTestExpectation *expectation))testBlock {
    XCTestExpectation *expectation = [self expectationWithDescription:description];
    
    if (testBlock) {
        testBlock(expectation);
    }
    
    [self waitForExpectationsWithTimeout:wait handler:^(NSError * _Nullable error) {
        if (error) {
            XCTFail(@"Expectation failed with error %@", error);
        }
    }];
}

@end
