//
//  RSBRequestValidationTests.m
//  KosHero
//
//  Created by Anton Kovalev on 03.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RSBRequest.h"
#import <OHHTTPStubs.h>
#import <OHHTTPStubsResponse+JSON.h>
#import <AFNetworking/AFNetworking.h>
#import "RSBBaseTests.h"

@interface RSBRequestValidationTests : RSBBaseTests

@property (nonatomic) RSBRequest *request;

@end

@implementation RSBRequestValidationTests

- (void)setUp {
    [super setUp];
    
    self.request = [RSBRequest request];
    self.request.sessionManager = [AFHTTPSessionManager manager];
}

- (void)tearDown {
    self.request = nil;
    
    [super tearDown];
}

- (void)testThatResponseHandleNilResponse {
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        id<OHHTTPStubsDescriptor> descriptior = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil]];
        
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(id  _Nullable response, NSError * _Nullable error) {
            XCTAssertNotNil(error);
            XCTAssertEqual(error.code, 415);
            [OHHTTPStubs removeStub:descriptior];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatResponseHandleStringResponse {
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        self.request.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        id<OHHTTPStubsDescriptor> descriptior = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithData:[@"String response" dataUsingEncoding:kCFStringEncodingUTF8] statusCode:200 headers:@{@"Content-Type":@"text/plain"}]];
        
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(id  _Nullable response, NSError * _Nullable error) {
            XCTAssertNotNil(error);
            XCTAssertEqual(error.code, 415);
            [OHHTTPStubs removeStub:descriptior];
            self.request.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatResponseHandleArrayResponse {
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        NSArray *sendedResponse = @[@1, @2, @3];
        id<OHHTTPStubsDescriptor> descriptior = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:sendedResponse statusCode:200 headers:nil]];
        
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(id  _Nullable response, NSError * _Nullable error) {
            XCTAssertNil(error);
            XCTAssertNotNil(response);
            [OHHTTPStubs removeStub:descriptior];
            [expectation fulfill];
        }];
    }];
}

- (void)testThatResponseHandleDictionaryResponse {
    [self performAsyncTestWithDescription:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] waitTime:1.0 testBlock:^(XCTestExpectation *expectation) {
        NSDictionary *sendedResponse = @{@"1":@"2", @"3":@"4"};
        id<OHHTTPStubsDescriptor> descriptior = [self stubHTTPRequestsWithResponse:[OHHTTPStubsResponse responseWithJSONObject:sendedResponse statusCode:200 headers:nil]];
        
        [self.request performWithMethod:@"GET" path:@"http://google.com" completionBlock:^(id  _Nullable response, NSError * _Nullable error) {
            XCTAssertNil(error);
            XCTAssertNotNil(response);
            [OHHTTPStubs removeStub:descriptior];
            [expectation fulfill];
        }];
    }];
}

@end
