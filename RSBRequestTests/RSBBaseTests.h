//
//  RSBBaseTests.h
//  KosHero
//
//  Created by Anton Kovalev on 03.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
@class OHHTTPStubsResponse;
@protocol OHHTTPStubsDescriptor;

@interface RSBBaseTests : XCTestCase

- (id<OHHTTPStubsDescriptor>)stubHTTPRequestsWithResponse:(OHHTTPStubsResponse *)response;
- (void)performAsyncTestWithDescription:(NSString *)description waitTime:(NSTimeInterval)wait testBlock:(void(^)(XCTestExpectation *expectation))testBlock;

@end
