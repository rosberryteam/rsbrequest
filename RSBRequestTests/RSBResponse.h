//
//  RSBResponse.h
//  KosHero
//
//  Created by Anton Kovalev on 04.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSBResponse : NSObject

@property (nonatomic) id object;

@end
