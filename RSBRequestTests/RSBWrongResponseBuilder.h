//
//  RSBWrongResponseBuilder.h
//  KosHero
//
//  Created by Anton Kovalev on 04.08.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBResponseBuilder.h"

@interface RSBWrongResponseBuilder : NSObject <RSBResponseBuilder>

@end
