//Copyright (c) 2016 RSBRequest
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <Foundation/Foundation.h>
@protocol RSBResponseBuilder;
@protocol RSBResponseErrorsHandler;
@protocol RSBResponseValidator;

@protocol RSBRequestHeaders;
@protocol RSBRequestParameters;

@class AFHTTPSessionManager;

NS_ASSUME_NONNULL_BEGIN

/**
 Domain of base validation errors such as receiving root object as nil or as not a dictionary or an array.
 */
extern NSString * const RSBResponseValidationErrorDomain;

/**
 This class provides clean flow of performing network requests with
 clear division of responsibilities such as validation, building response
 and errors handling.
 */
@interface RSBRequest<__covariant ResponseType> : NSObject

/**
 Response block invokes on finish of request performing.

 @param response Response object created from response by response builder. @see RSBResponseBuilder.
 @param error    If request fails you will receive error. This error may be differ from original one. @see RSBResponseValidator.
 
 @warning Request may not invoke completion block if you set up response errors handlers. @see RSBResponseErrorsHandler.
 */
typedef void(^RSBResponseBlock)(ResponseType _Nullable response, NSError * _Nullable error);

/**
 Download block invokes on finish of request performing.

 @param response Response received among file downloading.
 @param error    If request fails you will receive error.
 @param filePath Path to downloaded file.
 */
typedef void(^RSBDownloadBlock)(NSURLResponse * __null_unspecified response, NSURL * __null_unspecified filePath, NSError * __null_unspecified error);

/**
 Convenient initializer.

 @return Instance of RSBRequest.
 */
+ (instancetype)request;

///Use this property if request doesn't need validation (for example logout request always should logout user).
@property (nonatomic) BOOL shouldSkipValidation;

/**
 AFNetworking session manager for this request.
 */
@property (nonatomic) AFHTTPSessionManager *sessionManager;
/**
 Response builder for this request.
 @see RSBResponseBuilder.
 */
@property (nonatomic) id<RSBResponseBuilder> responseBuilder;
/**
 Array of response validators for this request.
 @see RSBResponseValidator.
 */
@property (nonatomic) NSArray<id<RSBResponseValidator>> *responseValidators;
/**
 Array of response errors handlers for this request.
 @see RSBResponseErrorsHandler
 */
@property (nonatomic) NSArray<id<RSBResponseErrorsHandler>> *responseErrorsHandlers;

/**
 Headers of this request.
 */
@property (nonatomic) id<RSBRequestHeaders> headers;
/**
 Parameters of this request.
 */
@property (nonatomic) id<RSBRequestParameters> parameters;
/**
 Path components of this request. 
 You can use any object as path components. Actually it's just stored in this property
 and can be used before request performing for creating hard paths like
 @c /user/{id}/follow/
 */
@property (nonatomic) id pathComponents;

/**
 This method begins request performing

 @param method          Method of request (GET, POST, etc.).
 @param path            Relative path of this request.
 @param completionBlock Completion block will be called when request will finish itself performing.
 
 @warning Request may not invoke completion block if you set up response errors handlers. @see RSBResponseErrorsHandler.
 */
- (void)performWithMethod:(NSString *)method
                     path:(NSString *)path
          completionBlock:(RSBResponseBlock)completionBlock;

/**
 This method begins file download process.

 @param request         URL request for file downloading.
 @param progress        Progress block.
 @param destination     Block for setting up destination file url. After downloading file will be moved from temporary directory to destination.
 @param completionBlock Completion block will be called when request will finish itself performing.
 */
- (void)performDownloadWithRequest:(NSURLRequest *)request
                          progress:(void (^)(NSProgress *progress))progress
                       destination:(NSURL *(^)(NSURL *targetPath, NSURLResponse *response))destination
                   completionBlock:(RSBDownloadBlock)completion;

/**
 This method resumes file download process.

 @param data            NSData for which resume will be started.
 @param progress        Progress block.
 @param destination     Block for setting up destination file url. After downloading file will be moved from temporary directory to destination.
 @param completionBlock Completion block will be called when request will finish itself performing.
 */
- (void)resumeDownloadRequestWithData:(NSData *)data
                             progress:(void (^)(NSProgress *progress))progress
                          destination:(NSURL *(^)(NSURL *targetPath, NSURLResponse *response))destination
                      completionBlock:(RSBDownloadBlock)completion;

/**
 Cancel request performing.
 If completion block is setted up it will be invoked with related error.
 */
- (void)cancel;

@end

NS_ASSUME_NONNULL_END
