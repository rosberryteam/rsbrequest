//Copyright (c) 2016 RSBRequest
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import "RSBRequest.h"

#import <AFNetworking/AFNetworking.h>

#import "RSBResponseBuilder.h"
#import "RSBResponseValidator.h"
#import "RSBResponseErrorsHandler.h"

#import "RSBRequestHeaders.h"
#import "RSBRequestParameters.h"

@interface RSBRequest ()

@property (nonatomic, readonly) NSURL *baseURL;
@property (nonatomic) NSURLSessionTask *currentTask;

@end

NSString * const RSBResponseValidationErrorDomain = @"com.rosberry.response.validation";

@implementation RSBRequest

+ (instancetype)request {
    return [[self alloc] init];
}

#pragma mark -

- (void)performWithMethod:(NSString *)method
                     path:(NSString *)path
          completionBlock:(RSBResponseBlock)completionBlock {
    
    void (^successBlock)(NSURLSessionDataTask * _Nonnull, id _Nullable) = ^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error = [self validateResponse:responseObject];
        if (error) {
            [self handleError:error forTask:task withResponseObject:responseObject completionBlock:completionBlock];
        }
        else {
            id response = responseObject;
            if (self.responseBuilder) {
                response = [self.responseBuilder buildResponseWithResponseObject:responseObject];
            }
            
            if (self.shouldSkipValidation) {
                if (completionBlock) {
                    completionBlock(response, nil);
                }
            }
            else {
                BOOL errorOccured = NO;
                for (id<RSBResponseValidator> validator in self.responseValidators) {
                    NSError *error = [validator validateResponse:response];
                    if (error) {
                        [self handleError:error forTask:task withResponseObject:response completionBlock:completionBlock];
                        errorOccured = YES;
                        break;
                    }
                }
                
                if (!errorOccured) {
                    if (completionBlock) {
                        completionBlock(response, nil);
                    }
                }
            }
        }
    };
    void (^failureBlock)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull) = ^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (self.shouldSkipValidation) {
            if (completionBlock) {
                completionBlock(nil, error);
            }
        }
        else {
            [self handleError:error forTask:task withResponseObject:nil completionBlock:completionBlock];
        }
    };
    
    NSDictionary *headers = self.headers.dictionaryRepresentation;
    for (NSString *key in headers.allKeys) {
        [self.sessionManager.requestSerializer setValue:headers[key] forHTTPHeaderField:key];
    }
    
    NSDictionary *parameters = self.parameters.dictionaryRepresentation;
    NSString *lowercaseMethod = method.lowercaseString;
    
    if ([lowercaseMethod isEqualToString:@"get"]) {
        self.currentTask = [self.sessionManager GET:path parameters:parameters progress:nil success:successBlock failure:failureBlock];
    }
    else if ([lowercaseMethod isEqualToString:@"post"]) {
        void(^bodyConstructionBlock)(id<AFMultipartFormData> formData) = [self.parameters bodyConstructionBlock];
        if (bodyConstructionBlock) {
            self.currentTask = [self.sessionManager POST:path parameters:parameters constructingBodyWithBlock:bodyConstructionBlock progress:nil success:successBlock failure:failureBlock];
        }
        else {
            self.currentTask = [self.sessionManager POST:path parameters:parameters progress:nil success:successBlock failure:failureBlock];
        }
    }
    else if ([lowercaseMethod isEqualToString:@"put"]) {
        self.currentTask = [self.sessionManager PUT:path parameters:parameters success:successBlock failure:failureBlock];
    }
    else if ([lowercaseMethod isEqualToString:@"patch"]) {
        self.currentTask = [self.sessionManager PATCH:path parameters:parameters success:successBlock failure:failureBlock];
    }
    else if ([lowercaseMethod isEqualToString:@"delete"]) {
        self.currentTask = [self.sessionManager DELETE:path parameters:parameters success:successBlock failure:failureBlock];
    }
    else {
        NSAssert(NO, @"Method %@ not supported", method);
    }
}

- (void)performDownloadWithRequest:(NSURLRequest *)request
                          progress:(void(^)(NSProgress *progress))progress
                       destination:(NSURL *(^)(NSURL *targetPath, NSURLResponse *response))destination
                   completionBlock:(RSBDownloadBlock)completion {
    self.currentTask = [self.sessionManager downloadTaskWithRequest:request
                                                           progress:progress
                                                        destination:destination
                                                  completionHandler:completion];
    [self.currentTask resume];
}

- (void)resumeDownloadRequestWithData:(NSData *)data
                             progress:(void(^)(NSProgress *progress))progress
                          destination:(NSURL *(^)(NSURL *targetPath, NSURLResponse *response))destination
                      completionBlock:(RSBDownloadBlock)completion {
    self.currentTask = [self.sessionManager downloadTaskWithResumeData:data
                                                              progress:progress
                                                           destination:destination
                                                     completionHandler:completion];
    [self.currentTask resume];
}

- (void)cancel {
    [self.currentTask cancel];
}

- (void)handleError:(NSError *)error forTask:(NSURLSessionDataTask *)task withResponseObject:(id)responseObject completionBlock:(nonnull void (^)(id _Nullable, NSError * _Nullable))completionBlock {
    
    BOOL errorHandled = NO;
    
    for (id<RSBResponseErrorsHandler> handler in self.responseErrorsHandlers) {
        if ([handler handleError:&error forRequest:self fromTask:task]) {
            errorHandled = YES;
            break;
        }
    }
    
    if (!errorHandled) {
        if (completionBlock) {
            completionBlock(responseObject, error);
        }
    }
}

- (NSError *)validateResponse:(id)responseObject {
    NSError *error = nil;
    
    if (responseObject == nil || [responseObject isEqual:[NSNull null]]) {
        NSString *localizedDescription = NSLocalizedString(@"ResponseObject is nil", nil);
        error = [NSError errorWithDomain:RSBResponseValidationErrorDomain
                                    code:415
                                userInfo:@{NSLocalizedDescriptionKey:localizedDescription}];
    }
    else if (![responseObject isKindOfClass:[NSDictionary class]] && ![responseObject isKindOfClass:[NSArray class]]) {
        NSString *localizedDescription = NSLocalizedString(@"ResponseObject not a dictionary or array", nil);
        error = [NSError errorWithDomain:RSBResponseValidationErrorDomain
                                    code:415
                                userInfo:@{NSLocalizedDescriptionKey:localizedDescription}];
    }
    
    return error;
}

@end
