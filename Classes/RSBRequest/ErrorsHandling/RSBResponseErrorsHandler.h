//Copyright (c) 2016 RSBRequest
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <Foundation/Foundation.h>
@class RSBRequest;

/**
 By adopting this protocol class can be used as response errors handler. It will
 have rights to interrupt flow of request performing so it's completion block
 will never be called. It can be used for situations like suddenly invalidation 
 of user token or impossibility of usage app because of update of API.
 @note Usually you need to notify some objects about such errors and your class
 need to implement this mechanic by itself.
 */
@protocol RSBResponseErrorsHandler <NSObject>

/**
 Override this method to control flow of the request.

 @param error   Error is in-out parameter. @c In is initial error from server or reponse validator.
 @param request Full request object for which handling is neccessary.
 @param task    @c NSURLSessionDataTask object used by @c RSBRequest under the hood.

 @return @c YES if handler takes responsibility of request's flow performing and @c NO otherwise.
 */
- (BOOL)handleError:(NSError **)error forRequest:(RSBRequest *)request fromTask:(NSURLSessionDataTask *)task;

@end
