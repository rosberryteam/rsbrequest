//Copyright (c) 2016 RSBRequest
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import "RSBReachability.h"
#import <AFNetworking/AFNetworking.h>

NSString * const RSBReachabilityChangedNotification = @"RSBReachabilityChangedNotification";

static BOOL reachabilityStatusExist = NO;

@implementation RSBReachability

+ (void)startMonitoring {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusNotReachable || status == AFNetworkReachabilityStatusUnknown) {
            [[NSNotificationCenter defaultCenter] postNotificationName:RSBReachabilityChangedNotification object:@NO];
        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:RSBReachabilityChangedNotification object:@YES];
        }
        reachabilityStatusExist = YES;
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

+ (void)stopMonitoring {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    reachabilityStatusExist = NO;
}

+ (BOOL)reachable {
    if (reachabilityStatusExist) {
        return [AFNetworkReachabilityManager sharedManager].reachable;
    }
    else {
        [self startMonitoring];
        return YES;
    }
}

@end
