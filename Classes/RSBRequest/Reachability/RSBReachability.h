//Copyright (c) 2016 RSBRequest
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <Foundation/Foundation.h>

/**
 This notification will be sent when reachability status will
 be changed. Observe it via @c NSNotificationCenter.
 */
extern NSString * const RSBReachabilityChangedNotification;

/**
 Static class for convenient check and managing reachability.
 */
@interface RSBReachability : NSObject

/**
 Call this method for start reachability monitoring.
 Usually you will do it right after app launching.
 */
+ (void)startMonitoring;
/**
 Call this method for stop reachability monitoring.
 */
+ (void)stopMonitoring;
/**
 Use this method for checking if internet is reachable.

 @return YES or NO.
 */
+ (BOOL)reachable;

@end
